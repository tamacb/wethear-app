import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:weatherapp/services/location.dart';
import 'package:http/http.dart' as http;
import 'package:weatherapp/services/networking.dart';
import 'package:weatherapp/services/weather.dart';

class LocationScreen extends StatefulWidget {
  LocationScreen({this.locationWeather});
  final locationWeather;

  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  WeatherModel weather = new WeatherModel();
  int temperatur;
  int condition;
  String city;
  String weatherIcon;
  String weatherMessage;

  @override
  void initState() {
    super.initState();
    updateView(widget.locationWeather);
  }

  void updateView(dynamic weatherData) {
    setState(() {
      if (weatherData == null){
        temperatur = 0;
        weatherIcon = 'Error';
        weatherMessage = 'Error';
        city = 'Tidak ditemukan';
        return;
      }
      double temp = weatherData['main']['temp'];
      temperatur = temp.toInt();

      var condition = weatherData['weather'][0]['id'];

      weatherMessage = weather.getMessage(temperatur);
      weatherIcon = weather.getWeatherIcon(condition);
      city = weatherData['name'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        color: Colors.lime,
      ),
      constraints: BoxConstraints.expand(),
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                FlatButton(
                  onPressed: () async {
                    var weatherData = await weather.getLocationWeather();
                    updateView(weatherData);
                  },
                  child: Icon(
                    Icons.near_me,
                    size: 50.0,
                  ),
                ),
                FlatButton(
                  onPressed: () {},
                  child: Icon(
                    Icons.location_city,
                    size: 50.0,
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Row(
                children: <Widget>[
                  Text('$temperatur dereajat'),
                  Text(weatherIcon)
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 15.0),
              child: Text(
                '$weatherMessage waktu di $city',
                textAlign: TextAlign.right,
              ),
            )
          ],
        ),
      ),
    ));
  }
}
