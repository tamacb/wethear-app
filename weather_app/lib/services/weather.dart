import 'package:weatherapp/services/location.dart';
import 'package:weatherapp/services/networking.dart';

const apikey = '27268fdf0fbe5994998adf15e23088bd';
const openweathermap = 'http://api.openweathermap.org/data/2.5/weather';

class WeatherModel {

  Future<dynamic> getLocationWeather() async {

    Location location = new Location();
    await location.getCurrentLocation();

    NetworkHelper networkHelper = new NetworkHelper(
        '$openweathermap?lat=${location.latitude}&lon=${location.longitude}&appid=$apikey&units=metric');

    var weatherData = await networkHelper.getData();

    return weatherData;
  }

  String getWeatherIcon(int condition){
    if (condition < 300){
      return 'medung';
    } else if (condition < 400){
      return 'hujan';
    } else if (condition < 400){
      return 'hujan deras';
    } else if (condition < 400){
      return 'hujan es';
    } else if (condition < 400){
      return 'salju';
    } else if (condition == 800){
      return 'cerah';
    } else if (condition <= 800){
      return 'berawan';
    } else 'not found';
  }

  String getMessage(int temp){
    if (temp > 25){
      return 'waktunya es';
    } else if (temp > 20){
      return 'pakai baju pendek';
    } else if (temp < 10){
      return 'pakai sarung tangan';
    } else {
      return 'pakai baju hangat';
    }
  }
}